const spans = document.getElementsByTagName('span');
const toggleHidden = document.getElementById('toggle-hidden');
toggleHidden.addEventListener('change', event => {
  for (let span of spans) {
    span.hidden = !event.target.checked;
  }
});
